create table public.customer
(
  id serial not null constraint customer_pkey primary key,
  last_name varchar(20) not null,
  first_name varchar(20) not null
);
