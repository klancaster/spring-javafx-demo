package main;



import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
class HandleAddCustomerWithInvoice{

    @Autowired
    CustomerRepository customerRepository;

    @Autowired
    InvoiceRepository invoiceRepository;

    public void add(String lastName, String firstName, String productName, Double amount){
        Customer customer = new Customer(lastName, firstName);
        Invoice invoice = new Invoice(customer,productName,amount);
        customerRepository.save(customer);
        invoiceRepository.save(invoice);
    }

}