package main;

import javax.persistence.*;
import java.util.Objects;

@Entity
public class Invoice {
    private int id;
    private String product;
    private double amount;
    private Customer customer;

    public Invoice() {
    }

    public Invoice(Customer customer, String product, double amount) {
        this.product = product;
        this.amount = amount;

        this.customer = customer;
    }

    @ManyToOne
    @JoinColumn(name = "customer_id")
    public Customer getCustomer() {
        return customer;
    }

    public void setCustomer(Customer customer) {
        this.customer = customer;
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Basic
    @Column(name = "product", nullable = false, length = 24)
    public String getProduct() {
        return product;
    }

    public void setProduct(String product) {
        this.product = product;
    }

    @Basic
    @Column(name = "amount", nullable = false, precision = 0)
    public double getAmount() {
        return amount;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Invoice invoice = (Invoice) o;
        return id == invoice.id &&
                Double.compare(invoice.amount, amount) == 0 &&
                Objects.equals(product, invoice.product);
    }

    @Override
    public int hashCode() {

        return Objects.hash(id, product, amount);
    }
}
