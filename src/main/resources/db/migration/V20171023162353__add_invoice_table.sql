CREATE TABLE public.Invoice
(
  id SERIAL PRIMARY KEY,
  product VARCHAR(24) NOT NULL,
  amount FLOAT NOT NULL,
  customer_id INT,
  CONSTRAINT customer___fk FOREIGN KEY (customer_id) REFERENCES customer (id)
);